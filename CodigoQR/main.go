package main

import (
	"github.com/skip2/go-qrcode"
	"log"
)

func main() {
	// var img []byte
	content := "https://github.com/Christolml"
	err := qrcode.WriteFile(content, qrcode.Medium, 256, "qr.png")
	if err != nil {
		log.Fatal(err)
	}
}
