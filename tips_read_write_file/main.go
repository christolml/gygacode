package main

import (
	"fmt"
	"io/ioutil"
	"log"
)

func main() {

	/* LEER UN ARCHIVO
	el io.util se usa para leer el archivo con readfile el cual devuelve un un slice de bite y un error por si pasa algo cuando lo este leyendo el archivo  */
	content, err := ioutil.ReadFile("file.txt")
	if err != nil {
		log.Fatal(err)
	}
	// content es un slice de bites, para poderlo visualizar debemos hacerle un casting
	// fmt.Println(string(content))

	// con los tres puntos, le indico al compiladora que no trate al slice de bytes como un slice, si no que trate cada uno de los elementos del slice
	// ahora va añadir cada byte de este slice a nuestro slice de bytes de content que es el original
	content = append(content, []byte("\nPrueba de escritura")...) //forma 2 de un archivo que ya tengo le sobreescribo cosas nuevas

	/* ESCRIBIR UN ARCHIVO
	 */
	// nos devuelve un error si ocurre cuando escribia el archivo, writerfile necesita tres parametros, el archivo que va a escribir, que es lo que va a escribir
	// como un slice de bytes y los permisos, el 0644 le permite que si el archivo no exite el lo crea
	// err := ioutil.WriteFile("file.txt", []byte("Prueba de escritura"), 0644)
	err = ioutil.WriteFile("file.txt", content, 0644) //forma 2
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Todo correcto")

}
