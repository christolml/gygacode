package main

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
)

type Note struct {
	Title       string    `json:"title"` //lo que esta en `` son notaciones para poder manejar la estructura de una forma mas adecuado en el json
	Description string    `json:"description"`
	CreatedAt   time.Time `json:"created_at"`
}

//este es donde vamos a almacenar en memoria nuestras notas, viene siendo una simulacion de una BD
var noteStore = make(map[string]Note)

var id int

// GetNoteHandler - GET - /api/notes, devuelvee codificado en json todas las notas que estan en el mapa
func GetNoteHandler(w http.ResponseWriter, r *http.Request) {
	var notes []Note
	for _, valor := range noteStore {
		notes = append(notes, valor) //aqui se tienen almacenadas las notes que habia en el map noteStore
	}
	w.Header().Set("Content-Type", "application/json") //se esta creando una cabecera http, que le indica al navegador que se le devuelve un contenido de tipo json
	j, err := json.Marshal(notes)                      //codificando el slice de notes para pasarlo a json, j se convierte en un slice de byte
	if err != nil {
		panic(err)
	}

	w.WriteHeader(http.StatusOK) //se setea la cabecera
	w.Write(j)                   //se esta retornando la respuesta al usuario
}

// GetNoteHandler - GET - /api/notes,  aqui el usuario nos manda en el cuerpo de la peticion un json con la estructura Nota que vamos a crear
func PostNoteHandler(w http.ResponseWriter, r *http.Request) {
	var note Note
	//en body viene la peticion del usuario, aqui le estamos pasando lo que va a descodificar de lo que nos manda el usuario con el Request
	// Decode(&note) le estoy indicando que lo decodifique a la structura de note por medio de un puntero indicandole a que estructura
	err := json.NewDecoder(r.Body).Decode(&note)
	if err != nil {
		panic(err)
	}
	note.CreatedAt = time.Now() //agregamos la fecha en que se creo la nota
	id++
	k := strconv.Itoa(id) //me convierte mi id entero a string para pasarlo al map que ocupa un id
	noteStore[k] = note

	// se esta devolviendo el mismo objeto (json) que nos envio el usuario pero con mas datos, en este caso con la fecha,
	w.Header().Set("Content-Type", "application/json")
	j, err := json.Marshal(note)
	if err != nil {
		panic(err)
	}

	w.WriteHeader(http.StatusCreated)
	w.Write(j)

}

// PutNoteHandler - PUT - /api/notes,  es la que se llama cuando el usuario hace una actualizacion de una nota por medio de PUT
func PutNoteHandler(w http.ResponseWriter, r *http.Request) {
	/* 	lo que haremos en esta funcion sera extraer los valores de las variables que se envien por el query de la peticion usamos una funcion de mux
	   	que nos devuelve esas variables en un map */
	vars := mux.Vars(r) //Vars recibe como parametro un puntero a un Request	y r ya es eso
	k := vars["id"]     //se obtiene el id que mando el usuario
	var noteUpdate Note
	/* 	el json lo decodificamos a un formato de GO, en Decode le estamos pasando el
	puntero a la variable donde se van a pasar los datos ya decodificados */
	err := json.NewDecoder(r.Body).Decode(&noteUpdate)
	if err != nil {
		panic(err)
	}

	// se veriffica si existe el id, note es la nota vieja a la que se va actualizar
	if note, ok := noteStore[k]; ok {
		noteUpdate.CreatedAt = note.CreatedAt
		delete(noteStore, k)      //se borra la nota vieja del map, para introducir la nueva
		noteStore[k] = noteUpdate //nota actualizada
	} else {
		log.Printf("No encontramos el id %s", k)
	}

	w.WriteHeader(http.StatusNoContent)

}

// DeleteNoteHandler - PUT - /api/notes,  es la que se llama cuando el usuario elimina una nota por medio de DELETE
func DeleteNoteHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	k := vars["id"]

	if _, ok := noteStore[k]; ok {
		delete(noteStore, k)
	} else {
		log.Printf("No encontramos el id %s", k)
	}
	w.WriteHeader(http.StatusNoContent)
}

func main() {

	r := mux.NewRouter().StrictSlash(false)
	r.HandleFunc("/api/notes", GetNoteHandler).Methods("GET")
	r.HandleFunc("/api/notes", PostNoteHandler).Methods("POST")
	r.HandleFunc("/api/notes/{id}", PutNoteHandler).Methods("PUT")
	r.HandleFunc("/api/notes/{id}", DeleteNoteHandler).Methods("DELETE")

	server := &http.Server{
		Addr:           ":8080",
		Handler:        r,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}
	log.Println("Listening http://localhost:8080...")
	server.ListenAndServe()

}
