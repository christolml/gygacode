package main

//con net/http es el que nos va a permitir crear nuestro servidor web
import (
	"fmt"
	"net/http"
)

func main() {
	/* recibe como parametro un pattern de la ruta que sera como la guía para que el paquete http sepa cuando llamar a la funcion que pasaremos como manejador
	   el segundo parametro es el manejador(handler), en este caso es una funcion que funciona como manejador
	   w es el que usaremos para escribir las respuestas en el navedor y con r es el que trae toda la informacion que el cliente esta haciendo al servidor
	*/
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		// recibe como parametro un writer (donde va a escribir) y el segundo parametro es el mensaje que se va a mostrar
		fmt.Fprintf(w, "<h1>Holi mundo</h1>")

	})

	http.ListenAndServe(":8080", nil)

}
