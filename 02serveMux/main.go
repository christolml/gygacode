package main

import (
	"fmt"
	"log"
	"net/http"
	"time"
)

//  ----------------------SERVERMUX
// // es un enrutador de peticiones http y dependiendo de la ruta llama al correspondiente handler
// con HandleFunc es como un servermux interno de http y es utilizado cuando cramos nuestro servidor web con ListenAndServe, pero como no hemos
// creado un servermux es por eso que pasamos nil como segundo parametro, en el nil debe ir el servermux que creamos

//-----------------HANDLER
/*Un event handler es una rutina que se utiliza para tratar el evento, lo que permite que un programador escriba código que se ejecutará cuando ocurra el evento.

Un handler es una rutina / función / método que se especializa en cierto tipo de datos o se centra en ciertas tareas especiales.
Ejemplos:
    *event handler: recibe y digiere eventos y señales del sistema circundante (por ejemplo, sistema operativo o GUI).
    *memory handler: realiza ciertas tareas especiales en la memoria.
    *file input handler una función que recibe entrada de archivos y realiza tareas especiales en los datos, todo dependiendo del contexto del curso.
*/

func holaMundo(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "<h1>Holi mundo desde un ServeMux creado por mi</h1>")
}

func prueba(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "<h1>Hola desde /prueba</h1>")
}

func usuario(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "<h1>Holi desde /usuario</h1>")
}

type mensaje struct {
	msg string
}

// handler casero implementando responseWriter y request, este es un manejador (handler)
func (m mensaje) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, m.msg)
}

func main() {
	// cuando usamos HandleFunc con http estamos usando el servermux interno

	msg := mensaje{
		msg: "Holi mundo de nuevo",
	}

	mux := http.NewServeMux()

	// HandleFunc es una funcion que trabaja como handler
	mux.HandleFunc("/", holaMundo)

	mux.HandleFunc("/prueba", prueba)

	mux.HandleFunc("/usuario", usuario)

	// se esta usando el handler que se hizo
	mux.Handle("/hola", msg)

	// se puede pasar el servermux como parametro ya que servermux implementa la interfaz handler
	// http.ListenAndServe(":8080", mux)

	// se elimina la llamada a ListenAndServe del paquete http que creaba un servidor interno,

	// se esta creando la estructura server, podemos modificar parametros a nuestro antojo
	server := &http.Server{ //con el & se indica que queremos crear un puntero de una estructura Server
		Addr:           ":8080",
		Handler:        mux,
		ReadTimeout:    10 * time.Second, //tiempo de lectura, tratando de leer la peticion es de 10 seg
		WriteTimeout:   10 * time.Second, //tiempo de escritura, dando la respuesta 10 seg   pasando estos tiempos se cancela la petición
		MaxHeaderBytes: 1 << 20,          //tamaño de la cabecera, en este es de 1 mb
	}

	log.Println("Listening...")
	//server.ListenAndServe() //no se pasan parametros ya que el puerto y el handler estan declarados en la estructura
	log.Fatal(server.ListenAndServe())

}
