package main

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
)

func GetUsers(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Mensaje desde el metodo GET")
}

func POSTUsers(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Mensaje desde el metodo POST")
}

func PUTUsers(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Mensaje desde el metodo PUT")
}

func DELETEUsers(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Mensaje desde el metodo DELETE")
}

func main() {
	r := mux.NewRouter().StrictSlash(false)
	/* 	con el StrictSlash(false) nos aseguramos que las rutas de abajo sean consideradas diferentes
	   	si estuviera en true el enrutador las toma como si fueran iguales
	   	/api/user
	   	/api/user/ */
	r.HandleFunc("/api/users", GetUsers).Methods("GET") //Ruta igual que las demas de abajo pero lee usuarios con el metodo GET
	r.HandleFunc("/api/users", POSTUsers).Methods("POST")
	r.HandleFunc("/api/users", PUTUsers).Methods("PUT")
	r.HandleFunc("/api/users", DELETEUsers).Methods("DELETE")

	server := &http.Server{
		Addr:           ":8080",
		Handler:        r,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}
	log.Println("Listening...")
	server.ListenAndServe()

}
