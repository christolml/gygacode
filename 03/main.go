package main

import (
	"fmt"
	"log"
	"net/http"
	"time"
)

func prueba(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "<h1>Hola desde /prueba</h1>")
}

func usuario(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "<h1>Holi desde /usuario</h1>")
}

type mensaje struct {
	msg string
}

func (m mensaje) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, m.msg)
}

func main() {

	msg := mensaje{
		msg: "Holi mundo de nuevo",
	}

	mux := http.NewServeMux()

	// con FileServer con pasarle el parametro de la ubicacion del directorio donde va tener lo archivos,
	// se encarga de hacer todo para que de una manera facil se puedan servir archivos estátivos
	fs := http.FileServer(http.Dir("public"))

	mux.Handle("/", fs)
	mux.HandleFunc("/prueba", prueba)
	mux.HandleFunc("/usuario", usuario)
	mux.Handle("/hola", msg)

	server := &http.Server{
		Addr:           ":8080",
		Handler:        mux,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	log.Println("Listening...")
	log.Fatal(server.ListenAndServe())

	// DIFERENCIAS ENTRE HANDLERS

	/*
			HANDLE Y HANDLER
			son funciones del metodo http, handle es cunado le indicamos un manejador al servermux y es enrutador ya es un handler ya
			que aplica la interfaz handler, cuando se ve handle se esta llamando al metodo que indica que utilice un manejador, cuando se ve
			handler se esta haciendo referencia a la interfaz handle de http

			HANDLEFUNC Y HANDLERFUNC
			si tiene la r se refiere a la interfaz, handlefunc es una funcion de http que recibe una funcion que implemente la firma


		http.Handle()
		http.HandleFunc()

		http.Handler
		http.HandlerFunc
	*/

}
