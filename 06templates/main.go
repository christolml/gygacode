package main

import (
	"os"
	// "html/template"
	"text/template"
)

type Persona struct {
	Nombre string
	Edad   int
}

/*se crea el texto que sera el template el cual se va a procesar y se le va insertar los datos e la estructuta de Persona
con {{.Nombre}} le indicamos que ahi debe apareceer el dato que sera parseado mas adelante
con {{range .}} estoy aplicando un for range dentro del template para despues pueda imprimir los datos que se le envien,
el range . el punto viene siendo el dato que le mandamos con el for range
con {{end}} se le indica el cierre  de llaves al range
lo de abajo vendria siendo un for range normal ejecutado en un template, con los datos que le mandeoms
*/
const tp = `
{{range .}}
	{{if .Edad}}
		Nombre: {{.Nombre}} Edad: {{.Edad}} - Correcto
	{{else}}
		Nombre: {{.Nombre}} Edad: {{.Edad}} - Incorrecto
	{{end}}
{{end}}`

func main() {

	persona := []Persona{
		{"Christopher", 21},
		{"La rana", 0},
		{"Esmeralda", 0},
	}
	/*	se crea un objeto del tipo template "La funcion New() del paquete template devuelve un puntero del tipo Template"
		"template_persona" es un nombre cualquier que se le pone al template*/
	t := template.New("template_persona")

	/*se esta parseando el template (tp) que tenemos texto, en pocas palabras a t se le esta dando el formato
	del template tp para que despues ya pueda recibir los datos de acuerdo al template*/
	t, err := t.Parse(tp)

	if err != nil {
		panic(err)
	}

	/*se ejecuta el template, t.Execute(os.Stdout, persona) el primer parametro es donde un ride a donde se va a mandar
	los datos ya parseado y la estructura que es de donde e van a obtener los datos,
	os.Stdout es la salido por defecto del sistema y en este caso es la terminal*/
	err = t.Execute(os.Stdout, persona)
	if err != nil {
		panic(err)
	}

}
